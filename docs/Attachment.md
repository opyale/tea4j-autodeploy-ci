# Attachment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**browserDownloadUrl** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**downloadCount** | **Long** |  |  [optional]
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**size** | **Long** |  |  [optional]
**uuid** | **String** |  |  [optional]
