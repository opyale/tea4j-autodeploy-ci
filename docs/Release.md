# Release

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | [**List&lt;Attachment&gt;**](Attachment.md) |  |  [optional]
**author** | [**User**](User.md) |  |  [optional]
**body** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**draft** | **Boolean** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**prerelease** | **Boolean** |  |  [optional]
**publishedAt** | [**Date**](Date.md) |  |  [optional]
**tagName** | **String** |  |  [optional]
**tarballUrl** | **String** |  |  [optional]
**targetCommitish** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**zipballUrl** | **String** |  |  [optional]
