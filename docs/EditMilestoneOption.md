# EditMilestoneOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  |  [optional]
**dueOn** | [**Date**](Date.md) |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
