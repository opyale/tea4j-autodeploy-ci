# Comment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**issueUrl** | **String** |  |  [optional]
**originalAuthor** | **String** |  |  [optional]
**originalAuthorId** | **Long** |  |  [optional]
**pullRequestUrl** | **String** |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
