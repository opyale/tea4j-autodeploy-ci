# WatchInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**Date**](Date.md) |  |  [optional]
**ignored** | **Boolean** |  |  [optional]
**reason** | **Object** |  |  [optional]
**repositoryUrl** | **String** |  |  [optional]
**subscribed** | **Boolean** |  |  [optional]
**url** | **String** |  |  [optional]
