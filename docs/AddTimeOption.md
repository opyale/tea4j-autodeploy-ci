# AddTimeOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | [**Date**](Date.md) |  |  [optional]
**time** | **Long** | time in seconds | 
**userName** | **String** | User who spent the time (optional) |  [optional]
