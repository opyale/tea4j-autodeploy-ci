# Issue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignee** | [**User**](User.md) |  |  [optional]
**assignees** | [**List&lt;User&gt;**](User.md) |  |  [optional]
**body** | **String** |  |  [optional]
**closedAt** | [**Date**](Date.md) |  |  [optional]
**comments** | **Long** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**dueDate** | [**Date**](Date.md) |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**isLocked** | **Boolean** |  |  [optional]
**labels** | [**List&lt;Label&gt;**](Label.md) |  |  [optional]
**milestone** | [**Milestone**](Milestone.md) |  |  [optional]
**number** | **Long** |  |  [optional]
**originalAuthor** | **String** |  |  [optional]
**originalAuthorId** | **Long** |  |  [optional]
**pullRequest** | [**PullRequestMeta**](PullRequestMeta.md) |  |  [optional]
**ref** | **String** |  |  [optional]
**repository** | [**RepositoryMeta**](RepositoryMeta.md) |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**url** | **String** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
