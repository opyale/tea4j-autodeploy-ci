# TopicResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | [**Date**](Date.md) |  |  [optional]
**id** | **Long** |  |  [optional]
**repoCount** | **Long** |  |  [optional]
**topicName** | **String** |  |  [optional]
**updated** | [**Date**](Date.md) |  |  [optional]
