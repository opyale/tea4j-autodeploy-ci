# CommitMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | [**Date**](Date.md) |  |  [optional]
**sha** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
