## Tea4j-autodeploy


Adding this dependency to your project:
```groovy
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}

dependencies {
    implementation 'org.codeberg.opyale:tea4j-autodeploy:{commit-sha}'
}
```