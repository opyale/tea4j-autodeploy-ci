# Fully automated Gitea Java SDK generation pipeline

# This pipeline periodically generates code from a swagger template and pushes
# potential changes to this repository using git version control

pipeline:

  # Download all required tools
  prepare:
    image: alpine/curl
    commands:
      - curl -L https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.33/swagger-codegen-cli-3.0.33.jar > swagger-codegen-cli.jar
      - curl -L https://github.com/google/google-java-format/releases/download/v1.15.0/google-java-format-1.15.0-all-deps.jar > google-java-format.jar

  # Generate code and docs from official swagger template
  generate:
    image: openjdk:8
    commands:
      - >
        java
        -DsupportingFiles=true
        -Dapis -DapiTests=false
        -Dmodels -DmodelTests=false
        -jar swagger-codegen-cli.jar generate
        --input-spec https://raw.githubusercontent.com/go-gitea/gitea/master/templates/swagger/v1_json.tmpl
        --config config.json
        --lang java
        --output ./generated/

  # Format generated and custom code according to the Google Java Style Guide
  format:
    image: openjdk:11
    commands:
      - java -jar google-java-format.jar --replace $(find ./generated/ -name "*.java")
      - java -jar google-java-format.jar --replace $(find ./custom/ -name "*.java")

  # Recreate directories and copy files of previous pipeline step
  combine:
    image: alpine
    commands:
      - mkdir --verbose --parents ./src/main/java/ && cp --verbose --recursive ./generated/src/main/java/* ./src/main/java/
      - mkdir --verbose --parents ./docs/          && cp --verbose --recursive ./generated/docs/*          ./docs/
      - mkdir --verbose --parents ./src/           && cp --verbose --recursive ./custom/src/*              ./src/

  # Check if generated code can be compiled at language level 8
  build:
    image: maven:3-eclipse-temurin-8
    commands:
      - mvn clean compile

  # Upload updated code to repository
  publish:
    image: alpine/git
    environment:
      # Information that should be used when authoring a commit
      - GIT_AUTHOR_NAME=gitnex2
      - GIT_AUTHOR_EMAIL=gitnex2@noreply.codeberg.org
      - GIT_COMMIT_MESSAGE=Updating Gitea Java SDK
      # Basic information concerning the repo that
      - GITEA_HOST=codeberg.org
      - GITEA_REPOSITORY=opyale/tea4j-autodeploy
      - GITEA_BRANCH=master
      # Username and token that should be used to authenticate against the gitea instance
      # - GITEA_USERNAME=secret
      # - GITEA_TOKEN=secret
    secrets: [ gitea_username, gitea_token ]
    commands:
      # Setup git credentials and checkout target branch
      - git config user.name  "$${GIT_AUTHOR_NAME}"
      - git config user.email "$${GIT_AUTHOR_EMAIL}"
      - git checkout "$${GITEA_BRANCH}"
      # Stage all important files for commit
      - git add -A ./docs
      - git add -A ./src
      # If files have changed, create a new commit and push it to the branch this pipeline was started on
      - >
        if git commit --message "$${GIT_COMMIT_MESSAGE}"; then
          git remote set-url origin "https://$${GITEA_USERNAME}:$${GITEA_TOKEN}@$${GITEA_HOST}/$${GITEA_REPOSITORY}"
          git push origin "$${GITEA_BRANCH}"
        fi