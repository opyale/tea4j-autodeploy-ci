/*
 * Gitea API.
 * This documentation describes the Gitea API.
 *
 * OpenAPI spec version: {{AppVer | JSEscape | Safe}}
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package org.gitnex.tea4j.v2.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Objects;

/** CreateKeyOption options when creating a key */
@Schema(description = "CreateKeyOption options when creating a key")
public class CreateKeyOption implements Serializable {
  private static final long serialVersionUID = 1L;

  @SerializedName("key")
  private String key = null;

  @SerializedName("read_only")
  private Boolean readOnly = null;

  @SerializedName("title")
  private String title = null;

  public CreateKeyOption key(String key) {
    this.key = key;
    return this;
  }

  /**
   * An armored SSH key to add
   *
   * @return key
   */
  @Schema(required = true, description = "An armored SSH key to add")
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public CreateKeyOption readOnly(Boolean readOnly) {
    this.readOnly = readOnly;
    return this;
  }

  /**
   * Describe if the key has only read access or read/write
   *
   * @return readOnly
   */
  @Schema(description = "Describe if the key has only read access or read/write")
  public Boolean isReadOnly() {
    return readOnly;
  }

  public void setReadOnly(Boolean readOnly) {
    this.readOnly = readOnly;
  }

  public CreateKeyOption title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Title of the key to add
   *
   * @return title
   */
  @Schema(required = true, description = "Title of the key to add")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateKeyOption createKeyOption = (CreateKeyOption) o;
    return Objects.equals(this.key, createKeyOption.key)
        && Objects.equals(this.readOnly, createKeyOption.readOnly)
        && Objects.equals(this.title, createKeyOption.title);
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, readOnly, title);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateKeyOption {\n");

    sb.append("    key: ").append(toIndentedString(key)).append("\n");
    sb.append("    readOnly: ").append(toIndentedString(readOnly)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
