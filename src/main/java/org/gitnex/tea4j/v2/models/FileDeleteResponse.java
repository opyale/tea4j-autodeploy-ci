/*
 * Gitea API.
 * This documentation describes the Gitea API.
 *
 * OpenAPI spec version: {{AppVer | JSEscape | Safe}}
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package org.gitnex.tea4j.v2.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Objects;

/** FileDeleteResponse contains information about a repo&#x27;s file that was deleted */
@Schema(
    description = "FileDeleteResponse contains information about a repo's file that was deleted")
public class FileDeleteResponse implements Serializable {
  private static final long serialVersionUID = 1L;

  @SerializedName("commit")
  private FileCommitResponse commit = null;

  @SerializedName("content")
  private Object content = null;

  @SerializedName("verification")
  private PayloadCommitVerification verification = null;

  public FileDeleteResponse commit(FileCommitResponse commit) {
    this.commit = commit;
    return this;
  }

  /**
   * Get commit
   *
   * @return commit
   */
  @Schema(description = "")
  public FileCommitResponse getCommit() {
    return commit;
  }

  public void setCommit(FileCommitResponse commit) {
    this.commit = commit;
  }

  public FileDeleteResponse content(Object content) {
    this.content = content;
    return this;
  }

  /**
   * Get content
   *
   * @return content
   */
  @Schema(description = "")
  public Object getContent() {
    return content;
  }

  public void setContent(Object content) {
    this.content = content;
  }

  public FileDeleteResponse verification(PayloadCommitVerification verification) {
    this.verification = verification;
    return this;
  }

  /**
   * Get verification
   *
   * @return verification
   */
  @Schema(description = "")
  public PayloadCommitVerification getVerification() {
    return verification;
  }

  public void setVerification(PayloadCommitVerification verification) {
    this.verification = verification;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FileDeleteResponse fileDeleteResponse = (FileDeleteResponse) o;
    return Objects.equals(this.commit, fileDeleteResponse.commit)
        && Objects.equals(this.content, fileDeleteResponse.content)
        && Objects.equals(this.verification, fileDeleteResponse.verification);
  }

  @Override
  public int hashCode() {
    return Objects.hash(commit, content, verification);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FileDeleteResponse {\n");

    sb.append("    commit: ").append(toIndentedString(commit)).append("\n");
    sb.append("    content: ").append(toIndentedString(content)).append("\n");
    sb.append("    verification: ").append(toIndentedString(verification)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
