package org.gitnex.tea4j.v2.apis;

import java.util.Date;
import java.util.List;
import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.AccessToken;
import org.gitnex.tea4j.v2.models.CreateAccessTokenOption;
import org.gitnex.tea4j.v2.models.CreateEmailOption;
import org.gitnex.tea4j.v2.models.CreateGPGKeyOption;
import org.gitnex.tea4j.v2.models.CreateKeyOption;
import org.gitnex.tea4j.v2.models.CreateOAuth2ApplicationOptions;
import org.gitnex.tea4j.v2.models.CreateRepoOption;
import org.gitnex.tea4j.v2.models.DeleteEmailOption;
import org.gitnex.tea4j.v2.models.Email;
import org.gitnex.tea4j.v2.models.GPGKey;
import org.gitnex.tea4j.v2.models.InlineResponse2001;
import org.gitnex.tea4j.v2.models.OAuth2Application;
import org.gitnex.tea4j.v2.models.PublicKey;
import org.gitnex.tea4j.v2.models.Repository;
import org.gitnex.tea4j.v2.models.StopWatch;
import org.gitnex.tea4j.v2.models.Team;
import org.gitnex.tea4j.v2.models.TrackedTime;
import org.gitnex.tea4j.v2.models.User;
import org.gitnex.tea4j.v2.models.UserHeatmapData;
import org.gitnex.tea4j.v2.models.UserSettings;
import org.gitnex.tea4j.v2.models.UserSettingsOptions;
import retrofit2.Call;
import retrofit2.http.*;

public interface UserApi {
  /**
   * Create a repository
   *
   * @param body (optional)
   * @return Call&lt;Repository&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("user/repos")
  Call<Repository> createCurrentUserRepo(@retrofit2.http.Body CreateRepoOption body);

  /**
   * Get user settings
   *
   * @return Call&lt;List&lt;UserSettings&gt;&gt;
   */
  @GET("user/settings")
  Call<List<UserSettings>> getUserSettings();

  /**
   * Get a Token to verify
   *
   * @return Call&lt;String&gt;
   */
  @GET("user/gpg_key_token")
  Call<String> getVerificationToken();

  /**
   * Update user settings
   *
   * @param body (optional)
   * @return Call&lt;List&lt;UserSettings&gt;&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("user/settings")
  Call<List<UserSettings>> updateUserSettings(@retrofit2.http.Body UserSettingsOptions body);

  /**
   * Add email addresses
   *
   * @param body (optional)
   * @return Call&lt;List&lt;Email&gt;&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("user/emails")
  Call<List<Email>> userAddEmail(@retrofit2.http.Body CreateEmailOption body);

  /**
   * Check if one user is following another user
   *
   * @param follower username of following user (required)
   * @param followee username of followed user (required)
   * @return Call&lt;Void&gt;
   */
  @GET("users/{follower}/following/{followee}")
  Call<Void> userCheckFollowing(
      @retrofit2.http.Path("follower") String follower,
      @retrofit2.http.Path("followee") String followee);

  /**
   * creates a new OAuth2 application
   *
   * @param body (required)
   * @return Call&lt;OAuth2Application&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("user/applications/oauth2")
  Call<OAuth2Application> userCreateOAuth2Application(
      @retrofit2.http.Body CreateOAuth2ApplicationOptions body);

  /**
   * Create an access token
   *
   * @param username username of user (required)
   * @param body (optional)
   * @return Call&lt;AccessToken&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("users/{username}/tokens")
  Call<AccessToken> userCreateToken(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Body CreateAccessTokenOption body);

  /**
   * Check whether a user is followed by the authenticated user
   *
   * @param username username of followed user (required)
   * @return Call&lt;Void&gt;
   */
  @GET("user/following/{username}")
  Call<Void> userCurrentCheckFollowing(@retrofit2.http.Path("username") String username);

  /**
   * Whether the authenticated is starring the repo
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Void&gt;
   */
  @GET("user/starred/{owner}/{repo}")
  Call<Void> userCurrentCheckStarring(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Unfollow a user
   *
   * @param username username of user to unfollow (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("user/following/{username}")
  Call<Void> userCurrentDeleteFollow(@retrofit2.http.Path("username") String username);

  /**
   * Remove a GPG key
   *
   * @param id id of key to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("user/gpg_keys/{id}")
  Call<Void> userCurrentDeleteGPGKey(@retrofit2.http.Path("id") Long id);

  /**
   * Delete a public key
   *
   * @param id id of key to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("user/keys/{id}")
  Call<Void> userCurrentDeleteKey(@retrofit2.http.Path("id") Long id);

  /**
   * Unstar the given repo
   *
   * @param owner owner of the repo to unstar (required)
   * @param repo name of the repo to unstar (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("user/starred/{owner}/{repo}")
  Call<Void> userCurrentDeleteStar(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Get a GPG key
   *
   * @param id id of key to get (required)
   * @return Call&lt;GPGKey&gt;
   */
  @GET("user/gpg_keys/{id}")
  Call<GPGKey> userCurrentGetGPGKey(@retrofit2.http.Path("id") Long id);

  /**
   * Get a public key
   *
   * @param id id of key to get (required)
   * @return Call&lt;PublicKey&gt;
   */
  @GET("user/keys/{id}")
  Call<PublicKey> userCurrentGetKey(@retrofit2.http.Path("id") Long id);

  /**
   * List the authenticated user&#x27;s followers
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("user/followers")
  Call<List<User>> userCurrentListFollowers(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the users that the authenticated user is following
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("user/following")
  Call<List<User>> userCurrentListFollowing(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the authenticated user&#x27;s GPG keys
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;GPGKey&gt;&gt;
   */
  @GET("user/gpg_keys")
  Call<List<GPGKey>> userCurrentListGPGKeys(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the authenticated user&#x27;s public keys
   *
   * @param fingerprint fingerprint of the key (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;PublicKey&gt;&gt;
   */
  @GET("user/keys")
  Call<List<PublicKey>> userCurrentListKeys(
      @retrofit2.http.Query("fingerprint") String fingerprint,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the repos that the authenticated user owns
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Repository&gt;&gt;
   */
  @GET("user/repos")
  Call<List<Repository>> userCurrentListRepos(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * The repos that the authenticated user has starred
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Repository&gt;&gt;
   */
  @GET("user/starred")
  Call<List<Repository>> userCurrentListStarred(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * List repositories watched by the authenticated user
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Repository&gt;&gt;
   */
  @GET("user/subscriptions")
  Call<List<Repository>> userCurrentListSubscriptions(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * Create a GPG key
   *
   * @param body (optional)
   * @return Call&lt;GPGKey&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("user/gpg_keys")
  Call<GPGKey> userCurrentPostGPGKey(@retrofit2.http.Body CreateGPGKeyOption body);

  /**
   * Create a public key
   *
   * @param body (optional)
   * @return Call&lt;PublicKey&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("user/keys")
  Call<PublicKey> userCurrentPostKey(@retrofit2.http.Body CreateKeyOption body);

  /**
   * Follow a user
   *
   * @param username username of user to follow (required)
   * @return Call&lt;Void&gt;
   */
  @PUT("user/following/{username}")
  Call<Void> userCurrentPutFollow(@retrofit2.http.Path("username") String username);

  /**
   * Star the given repo
   *
   * @param owner owner of the repo to star (required)
   * @param repo name of the repo to star (required)
   * @return Call&lt;Void&gt;
   */
  @PUT("user/starred/{owner}/{repo}")
  Call<Void> userCurrentPutStar(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * List the current user&#x27;s tracked times
   *
   * @param since Only show times updated after the given time. This is a timestamp in RFC 3339
   *     format (optional)
   * @param before Only show times updated before the given time. This is a timestamp in RFC 3339
   *     format (optional)
   * @return Call&lt;List&lt;TrackedTime&gt;&gt;
   */
  @GET("user/times")
  Call<List<TrackedTime>> userCurrentTrackedTimes(
      @retrofit2.http.Query("since") Date since, @retrofit2.http.Query("before") Date before);

  /**
   * delete an access token
   *
   * @param username username of user (required)
   * @param token token to be deleted, identified by ID and if not available by name (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("users/{username}/tokens/{token}")
  Call<Void> userDeleteAccessToken(
      @retrofit2.http.Path("username") String username, @retrofit2.http.Path("token") String token);

  /**
   * Delete email addresses
   *
   * @param body (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({"Content-Type:application/json"})
  @DELETE("user/emails")
  Call<Void> userDeleteEmail(@retrofit2.http.Body DeleteEmailOption body);

  /**
   * delete an OAuth2 Application
   *
   * @param id token to be deleted (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("user/applications/oauth2/{id}")
  Call<Void> userDeleteOAuth2Application(@retrofit2.http.Path("id") Long id);

  /**
   * Get a user
   *
   * @param username username of user to get (required)
   * @return Call&lt;User&gt;
   */
  @GET("users/{username}")
  Call<User> userGet(@retrofit2.http.Path("username") String username);

  /**
   * Get the authenticated user
   *
   * @return Call&lt;User&gt;
   */
  @GET("user")
  Call<User> userGetCurrent();

  /**
   * Get a user&#x27;s heatmap
   *
   * @param username username of user to get (required)
   * @return Call&lt;List&lt;UserHeatmapData&gt;&gt;
   */
  @GET("users/{username}/heatmap")
  Call<List<UserHeatmapData>> userGetHeatmapData(@retrofit2.http.Path("username") String username);

  /**
   * get an OAuth2 Application
   *
   * @param id Application ID to be found (required)
   * @return Call&lt;OAuth2Application&gt;
   */
  @GET("user/applications/oauth2/{id}")
  Call<OAuth2Application> userGetOAuth2Application(@retrofit2.http.Path("id") Long id);

  /**
   * List the authenticated user&#x27;s oauth2 applications
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;OAuth2Application&gt;&gt;
   */
  @GET("user/applications/oauth2")
  Call<List<OAuth2Application>> userGetOauth2Application(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get list of all existing stopwatches
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;StopWatch&gt;&gt;
   */
  @GET("user/stopwatches")
  Call<List<StopWatch>> userGetStopWatches(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the authenticated user&#x27;s access tokens
   *
   * @param username username of user (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;AccessToken&gt;&gt;
   */
  @GET("users/{username}/tokens")
  Call<List<AccessToken>> userGetTokens(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the authenticated user&#x27;s email addresses
   *
   * @return Call&lt;List&lt;Email&gt;&gt;
   */
  @GET("user/emails")
  Call<List<Email>> userListEmails();

  /**
   * List the given user&#x27;s followers
   *
   * @param username username of user (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("users/{username}/followers")
  Call<List<User>> userListFollowers(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the users that the given user is following
   *
   * @param username username of user (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("users/{username}/following")
  Call<List<User>> userListFollowing(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the given user&#x27;s GPG keys
   *
   * @param username username of user (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;GPGKey&gt;&gt;
   */
  @GET("users/{username}/gpg_keys")
  Call<List<GPGKey>> userListGPGKeys(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the given user&#x27;s public keys
   *
   * @param username username of user (required)
   * @param fingerprint fingerprint of the key (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;PublicKey&gt;&gt;
   */
  @GET("users/{username}/keys")
  Call<List<PublicKey>> userListKeys(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Query("fingerprint") String fingerprint,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the repos owned by the given user
   *
   * @param username username of user (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Repository&gt;&gt;
   */
  @GET("users/{username}/repos")
  Call<List<Repository>> userListRepos(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * The repos that the given user has starred
   *
   * @param username username of user (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Repository&gt;&gt;
   */
  @GET("users/{username}/starred")
  Call<List<Repository>> userListStarred(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List the repositories watched by a user
   *
   * @param username username of the user (required)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Repository&gt;&gt;
   */
  @GET("users/{username}/subscriptions")
  Call<List<Repository>> userListSubscriptions(
      @retrofit2.http.Path("username") String username,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List all the teams a user belongs to
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Team&gt;&gt;
   */
  @GET("user/teams")
  Call<List<Team>> userListTeams(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * Search for users
   *
   * @param q keyword (optional)
   * @param uid ID of the user to search for (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;InlineResponse2001&gt;
   */
  @GET("users/search")
  Call<InlineResponse2001> userSearch(
      @retrofit2.http.Query("q") String q,
      @retrofit2.http.Query("uid") Long uid,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * update an OAuth2 Application, this includes regenerating the client secret
   *
   * @param body (required)
   * @param id application to be updated (required)
   * @return Call&lt;OAuth2Application&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("user/applications/oauth2/{id}")
  Call<OAuth2Application> userUpdateOAuth2Application(
      @retrofit2.http.Body CreateOAuth2ApplicationOptions body, @retrofit2.http.Path("id") Long id);

  /**
   * Verify a GPG key
   *
   * @return Call&lt;GPGKey&gt;
   */
  @POST("user/gpg_key_verify")
  Call<GPGKey> userVerifyGPGKey();
}
