package org.gitnex.tea4j.v2.apis.custom;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

public interface WebApi {

  @Streaming
  @GET("{owner}/{repo}/pulls/{index}.diff")
  Call<ResponseBody> getPullDiffContent(
      @Path("owner") String owner, @Path("repo") String repo, @Path("index") String index);

  @Streaming
  @GET("{owner}/{repo}/raw/branch/{branch}/{filepath}")
  Call<ResponseBody> getFileContents(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("branch") String branch,
      @Path(value = "filepath", encoded = true) String filepath);

  @GET("{owner}/{repo}/git/commit/{sha}.{diffType}")
  Call<String> repoDownloadCommitDiffOrPatch(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("sha") String sha,
      @Path("diffType") String diffType);
}
